if [ $# -lt 2 ]; then
    echo "./clean-tc.sh <container name> <interface name>"
    exit 1
fi
sudo docker exec $1 tc qdisc del root dev $2 