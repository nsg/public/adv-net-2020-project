"""This can be used to play with tc without having to use RX.sh and SX.sh however
this can only be used during testing and not production since you are not allowed
to run any other python script than controller.py

This script was created for testing and thus it is not well conditioned to be 
directly used, but if you fancy it you can.

There are util functions to clean TC, or to set tc for a given node, or for all
nodes in the network, this will automatically use the topology object to get 
all the information about nodes ,intf names and bandwidths for each link.

It assumes that there are 3 bash scripts at the same path where you run this:

./clean-tc.sh <container name> <intf> (privided since it is standard)
./configure-tc.sh <router container> <intf name> <bw integer>
./configure-tc-switches.sh <switch container> <intf name> <bw integer>

As you can see to configure-tc* three parameters are passed container name, intf and bw
you can use those 3 arguments inside your "sinlge" interface TC configuration. for example:

rate=$3
sudo docker exec $1 tc qdisc add dev $2 root handle 1: htb default 30
sudo docker exec $1 tc class add dev $2 parent 1: classid 1:1 htb rate ${rate}Mbit ceil ${rate}Mbit burst 5k cburst 5k

$1 docker container name
$2 interface to configure
$2 rate to set rate and ceil in the above command

"""
import ipdb
import argparse
import csv
import subprocess

from p4utils.utils.topology import Topology


# network mapping, this could be a conf file
NODE_TO_CONTAINER = {
    "R1" : "1_R1router",
    "R2" : "1_R2router",
    "R3" : "1_R3router",
    "R4" : "1_R4router",
    "S1" : "1_S1router",
    "S2" : "1_S2router",
    "S3" : "1_S3router",
    "S4" : "1_S4router",
    "S5" : "1_S5router",
    "S6" : "1_S6router",    
    "h1" : "1_S1host",
    "h2" : "1_S2host",
    "h3" : "1_S3host",
    "h4" : "1_S4host",
    "h5" : "1_S5host",
    "h6" : "1_S6host"
}

class TCSetup(object):
    def __init__(self, topo):
        self.topo = Topology(db=topo)

    def show_node_itf_stats(self, node, intf):
        subprocess.call("sudo docker exec {} tc -s -d -p  class show dev {}".format(NODE_TO_CONTAINER[node], intf), shell=True)

    def get_intf_info(self, node):       
        interfaces_data = {}
        for intf, other_node in self.topo[node]["interfaces_to_node"].items():
            if intf == "host":
                continue
            bw = self.topo[node][other_node]["bw"]
            interfaces_data[intf] = bw
        return interfaces_data

    def get_nodes(self):
        return  self.topo.get_p4switches().keys() + self.topo.get_routers().keys()

    def clean_all_tc(self):
        nodes = self.get_nodes()
        for node in nodes:
            self.clean_node_tc(node)

    def clean_node_tc(self, node):
        intf_info = self.get_intf_info(node)
        for intf, _ in intf_info.items():
            subprocess.call("./clean-tc.sh {} {}".format(NODE_TO_CONTAINER[node], intf), shell=True)

    def set_switch_tc(self, sw):
        
        intf_info = self.get_intf_info(sw)
        for intf, bw in intf_info.items():
            subprocess.call("./configure-tc-switches.sh {} {} {}".format(NODE_TO_CONTAINER[sw], intf, bw), shell=True)

    def set_router_tc(self, router):
        intf_info = self.get_intf_info(router)
        for intf, bw in intf_info.items():
            subprocess.call("./configure-tc.sh {} {} {}".format(NODE_TO_CONTAINER[router], intf, bw), shell=True)

    def set_switches_tc(self):
        for switch in self.topo.get_switches().keys():
            self.set_switch_tc(switch)

    def set_routers_tc(self):
        for router in self.topo.get_routers().keys():
            self.set_router_tc(router)

    def set_nodes_tc(self):
        self.set_switches_tc()
        self.set_routers_tc()
    

if __name__ == "__main__":
    tc = TCSetup("/home/adv-net/infrastructure/build/topology.db")
